import { HttpClient } from "@angular/common/http";


export const GetCurrentTime = (http: HttpClient, success: Function, error: Function) => {
    const url = 'http://api.m.taobao.com/rest/api3.do?api=mtop.common.getTimestamp';
    http.get(url, {
        responseType: 'json',
    }).subscribe((res: any) => {
        success(parseInt(res.data.t));
    }, err => error(err));
};