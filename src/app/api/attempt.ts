import { HttpClient } from "@angular/common/http";
import * as $ from 'jquery';

export const RunAttempt = (http:HttpClient, attempt:string,pages:any,success:Function,error:Function)=>{
    const url = 'http://hubei.ouchn.cn/mod/quiz/attempt.php?attempt=' + attempt;
    const run = (page:number)=>{
        if(!pages[page]){
            success();
            return;
        }
        const realurl = url + '&page='+page;
        http.get(realurl,{
            withCredentials: true,
            responseType: 'text',
            observe: 'response',
        }).subscribe(res=>{
            const html =  $('<div>').html(res.body);
            const form = html.find('form#responseform');
            if(form.length===0){
                error('考核不存在');
                return;
            }
            const data = form.serializeArray();
            const f = new FormData();
            for(let i=0;i<data.length;i++){
                f.append(data[i].name, data[i].value);
            }
            http.post('http://hubei.ouchn.cn/mod/quiz/processattempt.php',f,{
                withCredentials: true,
                responseType: 'text',
                observe: 'response',
            }).subscribe(res=>{
                run(page+1);
            },err=>error('提交失败'));
        },err=>error("获取题目失败"));
    };
    run(0);
};


export const GetAttemptFormData = (http: HttpClient, attempt: string, formData: Array<any>, page: number = 0, success: Function, error: Function) => {
    let url = 'http://hubei.ouchn.cn/mod/quiz/attempt.php?attempt=' + attempt;
    if(page>0){
        url += '&page=' + page;
    }
    http.get(url, {
        withCredentials: true,
        responseType: 'text',
        observe: 'response',
    }).subscribe(res => {
        const html =  $('<div>').html(res.body);
        const form = html.find('form#responseform');
        const sessky = form.find('input[name=\'sesskey\']');
        const q = form.find('input[name^=\'q\'][id^=\'q\']');

        if (!sessky || sessky.length <= 0 || !q || q.length <= 0) {
            error('试题未找到');
            return;
        }

        let thisq = '';
        for (let i = 0; i < formData.length; i++) {
            const p = formData[i];
            const name = p.name;
            if (name.indexOf(':') > 0 && name[0] === 'q') {
                thisq = name.split(':')[0].substr(1);
                break
            }
        }

        const qid = q[0].attributes['name'].nodeValue.split(':')[0].substr(1);
        const sesskey = sessky.prop('value');
        const newFormData = [];
        for (let i = 0; i < formData.length; i++) {
            const fd = Object.assign({}, formData[i]);
            fd.name = fd.name.replace(thisq, qid);
            if (fd.name === 'sesskey') {
                fd.value = sesskey;
            } else if (fd.name === 'attempt') {
                fd.value = attempt;
            } else if (fd.name[0] === 'q' && fd.qtext) {
                continue;
            } else if (fd.name.indexOf('-seen')>=0){
                continue;
            }
            newFormData.push(fd);
        }
        for (let i = 0; i < formData.length; i++) {
            const fd = Object.assign({}, formData[i]);
            fd.name = fd.name.replace(thisq, qid);
            if (fd.qtext) {
                let qtexts = fd.qtext.replace('（', '（').replace('）', '）').trim().split('\n');
                let qtext = qtexts[0].trim();
                for(let j=0;j<qtexts.length;j++){
                    if(qtexts[j].trim().length>qtext.length){
                        qtext=qtexts[j].trim();
                    }
                }
                console.log('qtext',qtext);
                const qdiv = form.find("div.qtext:contains('" + qtext + "')");
                if (qdiv.length > 0) {
                    const pdiv = qdiv.closest('div.formulation.clearfix');
                    if (!fd.text) {
                        const inputs = pdiv.find('*[name=\''+fd.name+'\']')
                        for(let j=0;j<inputs.length;j++){
                            const ff = inputs[j];
                            newFormData.push({
                                name: ff.attributes['name'].nodeValue,
                                value:fd.value,
                            })
                        }

                        const format = pdiv.find('input[name*=\'answerformat\']')
                        for (let j = 0; j < format.length; j++) {
                            const ff = format[j];
                            newFormData.push({
                                name: ff.attributes['name'].nodeValue,
                                value: ff.attributes['value'].nodeValue,
                            });
                        }
                        const items = pdiv.find('input[name*=\'itemid\']')
                        for (let j = 0; j < items.length; j++) {
                            const ff = items[j];
                            newFormData.push({
                                name: ff.attributes['name'].nodeValue,
                                value: ff.attributes['value'].nodeValue,
                            });
                        }

                        const sequencechecks = pdiv.find("input[name*='sequencecheck']");
                        for (let k = 0; k < sequencechecks.length; k++) {
                            const sequencecheck = sequencechecks[k];
                            const name = sequencecheck.attributes['name'].nodeValue;
                            const value = sequencecheck.attributes['value'].nodeValue;
                            newFormData.push({ name, value });
                        }
                        const flaggeds = pdiv.find("input[name*='flagged'][value='0']");
                        for (let k = 0; k < flaggeds.length; k++) {
                            const flagged = flaggeds[k];
                            const name = flagged.attributes['name'].nodeValue;
                            const value = flagged.attributes['value'].nodeValue;
                            newFormData.push({ name, value });
                        }
                    } else {
                        let text = fd.text;
                        if (text.indexOf('. ') >= 0) {
                            let ss = text.split('. ');
                            text = ss.slice(1).join('. ');
                        }
                        const labels = pdiv.find("label:contains(" + text + ")");
                        if (labels.length > 0 && labels[0].attributes['for']) {
                            const forid = labels[0].attributes['for'].nodeValue;
                            const inputs = form.find("[id='" + forid + "']");
                            if (inputs.length > 0) {
                                const input = inputs.first();
                                const name = input.prop('name');
                                const value = input.prop('value');
                                fd.name = name;
                                if(value && fd.name.indexOf('choice')<0){
                                    fd.value = value;
                                }
                                newFormData.push(fd);

                                const sequencechecks = pdiv.find("input[name*='sequencecheck']");
                                for (let k = 0; k < sequencechecks.length; k++) {
                                    const sequencecheck = sequencechecks[k];
                                    const name = sequencecheck.attributes['name'].nodeValue;
                                    const value = sequencecheck.attributes['value'].nodeValue;
                                    newFormData.push({ name, value });
                                }
                                const flaggeds = pdiv.find("input[name*='flagged'][value='0']");
                                for (let k = 0; k < flaggeds.length; k++) {
                                    const flagged = flaggeds[k];
                                    const name = flagged.attributes['name'].nodeValue;
                                    const value = flagged.attributes['value'].nodeValue;
                                    newFormData.push({ name, value });
                                }
                                continue;
                            } else {
                                error('结果不匹配');
                                return;
                            }
                        } else {
                            error('回答不匹配');
                            return;
                        }
                    }
                } else {
                    console.log('题目不匹配', fd.qtext);
                    error('题目不匹配');
                    return;
                }
            }
        }

        const seen = form.find('input[name*="-seen"]');
        if(seen.length>0){
            newFormData.push({
                name: seen[0].attributes['name'].nodeValue,
                value: seen[0].attributes['value'].nodeValue,
            });
        }

        success(newFormData, sesskey);
    }, err => error('获取题目错误'));
};

export const ProcessAttempt = (http: HttpClient, param: Array<any>, success: Function, error: Function) => {
    const f = new FormData();
    for (let i = 0; i < param.length; i++) {
        const p = param[i];
        f.set(p.name, p.value);
    }
    const url = 'http://hubei.ouchn.cn/mod/quiz/processattempt.php';
    http.post(url, f, {
        responseType: 'text',
        observe: 'response',
        withCredentials: true,
    }).subscribe(res => {
        if (res.url.indexOf('summary.php?') <= 0 && res.url.indexOf('page=') <= 0) {
            error('提交失败');
            return;
        }
        success('ok');
    }, err => error('提交错误'));
};

export const FinishAttempt = (http: HttpClient, attempt: string, sesskey: string, success: Function, error: Function) => {
    const url = 'http://hubei.ouchn.cn/mod/quiz/processattempt.php';
    const f = new FormData();
    f.append('attempt', attempt);
    f.append('finishattempt', '1');
    f.append('sesskey', sesskey);
    f.append('slots', '');
    f.append('timeup', '0');
    http.post(url, f, {
        withCredentials: true,
        responseType: 'text',
        observe: 'response',
    }).subscribe(res => {
        if (res.url.indexOf('review') > 0) {
            success('ok');
        } else {
            error('失败');
        }
    }, err => error('未知错误'));
};