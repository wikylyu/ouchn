import { HttpClient } from '@angular/common/http';
import * as Tesseract from 'tesseract.js';
import * as $ from 'jquery';
const session = require('electron').remote.session;


export const GetLearnCourse = (http: HttpClient, success: Function, error: Function) => {
    const url = 'http://shome.ouchn.cn/Learn/Course';
    http.get(url, {
        responseType: 'text',
        observe: 'response',
        withCredentials: true,
    }).subscribe(res => {
        const html =  $('<div>').html(res.body);
        const as = html.find('a[coursecode][curriculumid]');
        const results = [];
        for (let i = 0; i < as.length; i++) {
            const a = as[i];
            const courseCode = a.attributes['coursecode'].nodeValue;
            const curriculumid = a.attributes['curriculumid'].nodeValue;
            results.push({
                courseCode,
                curriculumid,
            });
        }
        success(results);
    }, err => error('网络错误'));
};

export const LoginHubei = (http: HttpClient, courseCode: string, cid: string, success: Function, error: Function) => {
    const url = 'http://shome.ouchn.cn/Learn/Course/GetMoodleHub?site=http://hubei.ouchn.cn&rid=4&courseCode=' + courseCode + '&cid=' + cid;
    http.get(url, {
        responseType: 'text',
        observe: 'response',
        withCredentials: true,
    }).subscribe(res => {
        const html =  $('<div>').html(res.body);
        const form = html.find('form#moodlehub');
        const action = form.prop('action');
        if (!action) {
            error('登录湖北站失败');
        } else {
            const f = new FormData();
            const inputs = form.find('[name][value]');
            for (let i = 0; i < inputs.length; i++) {
                const input = inputs[i];
                f.append(input.attributes['name'].nodeValue, input.attributes['value'].nodeValue)
            }
            http.post(action, f, {
                responseType: 'text',
                observe: 'response',
                withCredentials: true,
            }).subscribe(res => {
                success('ok');
            }, err => error('网络错误'));
        }
    }, err => error('网络错误'));
}

export const Login = (http: HttpClient, username: string, password: string, success: Function, error: Function, seq: number = 0) => {

    session.defaultSession.clearStorageData({ storages: ['cookies'] }, () => {
        http.get('http://shome.ouchn.cn/', {
            responseType: 'text',
            observe: 'response',
            withCredentials: true,
        }).subscribe(res => {
            if (res.status !== 200) {
                error('登录失败');
                return;
            }
            const html =  $('<div>').html(res.body);
            const form = html.find('form[action*=\'Login\']');
            const action = form.prop('action');
            if (!action) {
                error('登录失败');
                return;
            }
            const query = action.split('?')[1];
            const url = 'http://sso.ouchn.cn/Passport/Login?' + query;

            /* 获取验证码 */
            http.get('http://sso.ouchn.cn/ashx/CheckCode.ashx', {
                responseType: 'blob',
                observe: 'response',
                withCredentials: true,
            }).subscribe(res1 => {
                Tesseract.recognize(res1.body).then((result) => {
                    const f = new FormData();
                    f.append('checkCode', result.text.substr(0, 4));
                    f.append('username', username);
                    f.append('password', password);
                    http.post(url, f, {
                        responseType: 'text',
                        observe: 'response',
                        withCredentials: true,
                    }).subscribe(res2 => {
                        if (res2.url === 'http://shome.ouchn.cn/') {
                            GetLearnCourse(http, (courses) => {
                                if (courses.length === 0) {
                                    error('课程未找到');
                                } else {
                                    console.log('courses', courses);
                                    const loginhubei = (index: number) => {
                                        const c = courses[index];
                                        if (!c) {
                                            success('ok');
                                            return;
                                        }
                                        LoginHubei(http, c.courseCode, c.curriculumid, () => {
                                            loginhubei(index + 1);
                                        }, error);
                                    }
                                    loginhubei(0);
                                }
                            }, err => error('网络错误'))
                        } else if (seq <= 3) {  /* 可能是密码错误，也可能是验证码错误，重试 */
                            Login(http, username, password, success, error, seq + 1);
                        } else {
                            error('登录失败');
                            return;
                        }
                    }, err => error('网络错误'));
                }).catch((e) => {
                    error('验证码解析失败');
                });
            }, err => error('网络错误'));
        }, err => error('网络错误'));
    });
}