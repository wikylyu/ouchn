import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { sep } from 'path';


export const GetQuizAttempt = (http: HttpClient, id: string, success: Function, error: Function) => {
    const url = 'http://hubei.ouchn.cn/mod/quiz/view.php?id=' + id;
    http.get(url, {
        responseType: 'text',
        observe: 'response',
        withCredentials: true,
    }).subscribe((res) => {
        const html =  $('<div>').html(res.body);
        const form = html.find('form[action*=\'startattempt\']');
        const action = form.prop('action');
        const cmid = form.find('input[name=\'cmid\']').prop('value');
        const sesskey = form.find('input[name=\'sesskey\']').prop('value');
        if (!action || !cmid || !sesskey) {
            error('考核未找到');
            return;
        }
        const f = new FormData();
        f.append('cmid', cmid);
        f.append('sesskey', sesskey);
        http.post(action, f, {
            responseType: 'text',
            observe: 'response',
            withCredentials: true,
        }).subscribe(res1 => {
            const i = res1.url.indexOf('attempt.php?attempt=');
            if (i <= 0) {
                error('考核未找到');
                return;
            }
            let attempt = res1.url.substr(i + 20);
            let seps = attempt.split('&');
            success(seps[0]);
        }, err => error('网络错误'));
    }, err => error('网络错误'));
};
