import { Component, OnInit, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Login } from '../../api/login';
import { Router } from '@angular/router';
import { GetQuizAttempt } from '../../api/quiz';
import { GetCurrentTime } from '../../api/time';

@Component({
    selector: 'app-login-page',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
    validateForm: FormGroup;

    status = 0;

    username = '';
    password = '';
    quizid = '';
    remember = false;
    isLoading = false;

    constructor(private fb: FormBuilder, private http: HttpClient, private message: NzMessageService,
        private router: Router, private zone: NgZone) {
        this.isLoading = false;
        this.status = 3;
    }

    ngOnInit(): void {
        this.validateForm = this.fb.group({
            userName: [null, [Validators.required]],
            password: [null, [Validators.required]],
            quizid: [null, [Validators.required]],
            remember: [true]
        });
        this.username = window.localStorage.getItem('username');
        this.password = window.localStorage.getItem('password');
        if (this.username || this.password) {
            this.remember = true;
        }

        // GetCurrentTime(this.http, (t) => {
        //     if (t < 1544371200000) {
        //         this.status = 3;
        //     } else {
        //         this.status = 1;
        //     }
        // }, err => {
        //     this.status = 2;
        // });
    }
    submitForm(): void {
        for (const i in this.validateForm.controls) {
            this.validateForm.controls[i].markAsDirty();
            this.validateForm.controls[i].updateValueAndValidity();
        }
        const { username, password, remember, quizid } = this;
        if (!username || !password || !quizid || this.isLoading) {
            return;
        }

        if (this.status === 0) {
            this.message.create('info', `等待网络链接`);
            return;
        } else if (this.status === 1) {
            this.message.create('error', `不能再使用测试版`);
            return;
        } else if (this.status === 2) {
            this.message.create('error', `网络错误，请检查网络后重启程序`);
            return;
        }
        console.log(username, password, remember, quizid);

        if (remember) {
            window.localStorage.setItem('username', username);
            window.localStorage.setItem('password', password);
        } else {
            window.localStorage.setItem('username', '');
            window.localStorage.setItem('password', '');
        }

        this.isLoading = true;
        Login(this.http, username, password, () => {
            this.checkQuiz(quizid);
        }, (e) => {
            this.zone.run(() => {
                this.isLoading = false;
                console.log('error', e);
                this.message.create('error', `登录失败，请重新登录。`);
            });
        });
    }

    checkQuiz(quizid): void {
        GetQuizAttempt(this.http, quizid, attempt => {
            this.zone.run(() => {
                this.isLoading = false;
                this.router.navigate(['main', { attempt, quizid }]);
            });
        }, err => {
            console.log('checkQuiz error', err);
            this.zone.run(() => {
                this.isLoading = false;
                this.message.create('error', '查询考试失败，请确认后重新输入。');
            });
        })
    }
}
