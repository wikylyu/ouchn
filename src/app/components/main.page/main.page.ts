import { Component, OnInit, NgZone, ElementRef } from '@angular/core';
import { NzMessageService, UploadFile, P } from 'ng-zorro-antd';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { ProcessAttempt, FinishAttempt, GetAttemptFormData, RunAttempt } from '../../api/attempt';
import * as XLSX from 'xlsx';
import { Login } from '../../api/login';
import { GetQuizAttempt } from '../../api/quiz';


@Component({
    selector: 'app-main-page',
    templateUrl: './main.page.html',
    styleUrls: ['./main.page.scss']
})
export class MainPage implements OnInit {

    private quizid: string;
    private attempt: string;
    private webviewRef: any;
    private working: boolean;
    src: string;
    showConfirmModal: boolean;

    formData: any;
    accounts = [];
    pages: any;
    wait: number = 5;

    constructor(private http: HttpClient, private message: NzMessageService, private router: Router,
        private zone: NgZone, private route: ActivatedRoute, private elementRef: ElementRef) {
    }

    ngOnInit(): void {
        this.attempt = this.route.snapshot.paramMap.get("attempt");
        this.quizid = this.route.snapshot.paramMap.get('quizid');
        this.working = false;
        this.pages = {};
    }

    ngAfterViewInit(): void {
        this.src = "http://hubei.ouchn.cn/mod/quiz/attempt.php?attempt=" + this.attempt + "&page=0";

        this.webviewRef = this.elementRef.nativeElement.querySelector('webview');
        this.webviewRef.addEventListener('ipc-message', event => {
            this.formData = event.channel || [];
            if (!this.formData || this.working) {
                return;
            }
            console.log(this.formData);
            let nextpage = -1;
            let thispage = 0;
            let next = '';
            for (let i = 0; i < this.formData.length; i++) {
                if (this.formData[i].name === 'nextpage') {
                    nextpage = parseInt(this.formData[i].value);
                } else if (this.formData[i].name === 'thispage') {
                    thispage = parseInt(this.formData[i].value);
                } else if (this.formData[i].name === 'next') {
                    next = this.formData[i].value;
                }
            }
            if (next === '下一页') {   /* 还有下一页 */
                this.zone.run(() => {
                    this.src = "http://hubei.ouchn.cn/mod/quiz/attempt.php?attempt=" + this.attempt + "&page=" + nextpage;
                    this.webviewRef.loadURL(this.src);
                });
                this.pages[thispage] = this.formData;
                return;
            } else if (next === '上一页') { /* 返回上一页 */
                this.zone.run(() => {
                    this.src = "http://hubei.ouchn.cn/mod/quiz/attempt.php?attempt=" + this.attempt + "&page=" + (thispage - 1);
                    this.webviewRef.loadURL(this.src);
                });
                this.pages[thispage] = this.formData;
                return;
            }
            if (Object.keys(this.pages).length > 0) {
                this.pages[thispage] = this.formData;
            }
            this.zone.run(() => {
                if (this.accounts.length === 0) {
                    this.message.create('info', '请添加学生帐号');
                    return;
                }
                this.wait = this.wait || 0;
                if (this.wait < 0) {
                    this.wait = 0;
                }
                this.showConfirmModal = true;
            });
        });
        this.webviewRef.addEventListener('dom-ready', () => {
            const js = `
            const form = $('#responseform');
            form.on('submit', function(e){
              e.preventDefault();
              var val = document.activeElement.getAttribute('value');
              const formData = form.serializeArray();
              for(let i=0;i<formData.length;i++){
                  if(formData[i].name[0]!=='q'){
                      continue;
                  }
                  let query = "[type!='hidden'][name='" + formData[i].name +"'][value='" + formData[i].value+ "']";
                  let inputs=$(this).find(query);
                  if(inputs.length<=0){
                      query = "[type!='hidden'][name='" + formData[i].name +"']";
                      inputs = $(this).find(query);
                      if(inputs.length<=0){
                        continue;
                      }
                  }
                  const input = inputs[0];
                  const labels = $(this).find("label[for='"+input.id+"']")
                  if(labels.length>0){
                    formData[i].text = labels.text();
                  }
                  const qtexts = inputs.closest('div.formulation.clearfix').find('div.qtext');
                  if(qtexts.length>0){
                    formData[i].qtext = qtexts.text();
                  }
              }
              formData.push({
                name: 'next',
                value: val,
              });
              window.ipcRenderer.sendToHost(formData);
            });
            $('a').on('click', function(e){
              e.preventDefault();
            })
            `;
            // this.webviewRef.openDevTools();
            this.webviewRef.executeJavaScript(js);
        });
    }

    handleOk = () => {
        this.showConfirmModal = false;
        if (this.working) {
            return;
        }
        this.working = true;
        this.message.create('info', '开始执行答题任务，请勿关闭程序。每个用户答题时间约为' + this.wait + '分钟', { nzDuration: 10000 });
        if (Object.keys(this.pages).length > 0) {
            console.log(this.pages);
            this.doPagesWork(0);
        } else {
            console.log(this.formData);
            this.doWork(0);
        }
    }

    /* 多页考试 */
    doPagesWork = (i) => {
        const account = this.accounts[i];
        if (!account) {
            this.working = false;
            this.message.create('success', '任务执行完毕', { nzDuration: 10000 });
            return;
        }
        account.icon = 'sync';
        account.message = '';
        const j = i + 1;
        const error = (err) => {
            console.log(err);
            this.zone.run(() => {
                account.icon = 'info';
                account.message = err;
                this.doPagesWork(j);
            })
        };
        Login(this.http, account.username, account.password, () => {
            GetQuizAttempt(this.http, this.quizid, (attempt) => {
                RunAttempt(this.http, attempt, this.pages, ()=>{
                    const process = (k, sesskey) => {
                        if (!this.pages[k]) {
                            if (!sesskey) {
                                error('未完成');
                                return;
                            }
                            let wait = Math.round(this.wait * 60 + (Math.random() - 0.5) * 60);
                            if (wait <= 0) {
                                wait = 1;
                            }
                            console.log('wait', wait);
                            setTimeout(() => {
                                // this.zone.run(()=>{
                                //     account.icon = 'check';
                                //     this.doPagesWork(j);
                                // });
    
                                FinishAttempt(this.http, attempt, sesskey, () => {
                                    this.zone.run(() => {
                                        account.icon = 'check';
                                        this.doPagesWork(j);
                                    })
                                }, error);
                            }, wait * 1000);
                            return;
                        }
                        GetAttemptFormData(this.http, attempt, this.pages[k], k, (formData, sesskey) => {
                            console.log(formData, sesskey);
                            ProcessAttempt(this.http, formData, () => {
                                process(k + 1, sesskey);
                            }, error);
                        }, error);
                    }
                    process(0, '');
                }, error);
            }, error)
        }, error);
    }

    /* 单页考试 */
    doWork = (i) => {
        const account = this.accounts[i];
        if (!account) {
            this.working = false;
            this.message.create('success', '任务执行完毕');
            return;
        }
        account.icon = 'sync';
        account.message = '';
        const j = i + 1;
        const error = (err) => {
            console.log(err);
            this.zone.run(() => {
                account.icon = 'info';
                account.message = err;
                this.doWork(j);
            })
        };
        Login(this.http, account.username, account.password, () => {
            GetQuizAttempt(this.http, this.quizid, (attempt) => {
                GetAttemptFormData(this.http, attempt, this.formData, 0, (formData, sesskey) => {
                    console.log(formData, sesskey);
                    let wait = Math.round(this.wait * 60 + (Math.random() - 0.5) * 60);
                    if (wait <= 0) {
                        wait = 1;
                    }
                    console.log('wait', wait);
                    setTimeout(() => {
                        ProcessAttempt(this.http, formData, () => {
                            FinishAttempt(this.http, attempt, sesskey, () => {
                                this.zone.run(() => {
                                    account.icon = 'check';
                                    this.doWork(j);
                                })
                            }, error);
                        }, error);
                    }, wait * 1000);
                }, error);
            }, error)
        }, error);
    }

    handleCancel = () => {
        this.showConfirmModal = false;
    }

    beforeUpload = (file: UploadFile): boolean => {
        if (this.working) {
            return;
        }
        const xls = XLSX.readFile(file.path)
        const rows = XLSX.utils.sheet_to_json(xls.Sheets['Sheet1']);
        const accounts = [];
        for (let i = 0; i < rows.length; i++) {
            const r = rows[i];
            const name = r['姓名'] || '';
            const username = r['学号'] || '';
            const id = r['证件号码'] || '';
            if (!username || !id) {
                continue;
            }
            const password = id.substr(6, 8);
            accounts.push({
                name: name,
                username: username,
                password: password,
                icon: 'ellipsis',
            });
        }
        this.accounts = accounts;
        return false;
    }

    goBack = () => {
        if (this.working) {
            return;
        }
        this.router.navigate(['']);
    }

}
