import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPage } from './components/login.page/login.page';
import { MainPage } from './components/main.page/main.page';

const routes: Routes = [
    {
        path: '',
        component: LoginPage,
        pathMatch: 'full',
    },
    {
        path: 'main',
        component: MainPage,
        pathMatch: 'full',
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true, enableTracing: false })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
