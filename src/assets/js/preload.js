const { ipcRenderer } = require('electron');
window.ipcRenderer = ipcRenderer;

window.addEventListener("load", function (event) {
    window.$ = window.jQuery = require('./jquery.min.js');
}, false);
